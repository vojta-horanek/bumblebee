//
// Created by vojta on 1/12/20.
//

#include "Map.h"

Map::Map(SDL2pp::Texture &background) {
    this->background = &background;
}

bool Map::Advance() {
    if (mapLength > currentX + mapHeight) {
        currentX++;
        renderMap.clear();
        for (auto &line : mapData) {
            renderMap.emplace_back(line.begin() + currentX, line.begin() + currentX + mapHeight);
        }
        return true;
    }
    return false;
}

void Map::ParseMap(const std::string &mapFileName) {
    std::ifstream mapFile(mapFileName);
    std::string line;
    while (std::getline(mapFile, line)) {
        std::vector<enum Block> mapLine;
        for (auto &ch : line) {
            enum Block b;
            switch (ch) {
                case 'B':
                case 'b':
                    b = Block::Block;
                    break;
                case 'S':
                case 's':
                    b = Block::Stop;
                    break;
                default:
                case 'A':
                case 'a':
                case ' ':
                    b = Block::Air;
                    break;
            }
            mapLine.push_back(b);
        }
        mapData.push_back(mapLine);
        mapLength = mapLine.size();
    }
    mapHeight = mapData.size();
    mapFile.close();
}
