//
// Created by vojta on 1/12/20.
//

#ifndef BUMBLEBEE_MAP_H
#define BUMBLEBEE_MAP_H

#include <fstream>
#include <string>
#include <SDL2pp/Texture.hh>

enum Block {
    Air, Block, Stop
};

class Map {
public:
    Map(SDL2pp::Texture &background);
    void ParseMap(const std::string &mapFile);
    bool Advance();
    SDL2pp::Texture *background;
    std::vector<std::vector<enum Block>> mapData;
    std::vector<std::vector<enum Block>> renderMap;
    Uint32 currentX = 0;
    Uint32 mapLength = 0;
    int mapHeight = 0;
};


#endif //BUMBLEBEE_MAP_H
