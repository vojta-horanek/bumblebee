//
// Created by vojta on 1/12/20.
//

#ifndef BUMBLEBEE_PLAYER_H
#define BUMBLEBEE_PLAYER_H

#include <SDL2/SDL.h>
#include <SDL2pp/Color.hh>

class Player {
public:
    Player();
    void HandleInput(SDL_Keycode keycode);
    void FlipGravity();
    int X;
    int Y;
    SDL2pp::Color color;
};

#endif //BUMBLEBEE_PLAYER_H
