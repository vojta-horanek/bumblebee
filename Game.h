//
// Created by vojta on 1/12/20.
//

#ifndef BUMBLEBEE_GAME_H
#define BUMBLEBEE_GAME_H


#include <SDL2pp/Renderer.hh>
#include "Player.h"
#include "Map.h"

class Game {
public:
    Game(SDL2pp::Renderer &renderer, int mapHeight);
    virtual ~Game();
    void RenderPlayer(const Player& player);
    void RenderMap(const Map& map);
private:
    SDL2pp::Renderer *renderer;
    int blockSize;
    SDL2pp::Texture *blockTexture;
    int renderStartY;
};


#endif //BUMBLEBEE_GAME_H
