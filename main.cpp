#include <iostream>

#include <SDL2/SDL.h>
#include <SDL2pp/SDL.hh>
#include <SDL2pp/SDL2pp.hh>
#include <SDL2pp/SDLTTF.hh>
#include "Player.h"
#include "Game.h"

#define FPS_CAP 60
#define TICKS_PER_FRAME 1000 / FPS_CAP

int main() {
    try {
        using namespace SDL2pp;

        SDL sdl(SDL_INIT_VIDEO | SDL_INIT_AUDIO);
        SDLTTF sdl_ttf;

        SDL_DisplayMode DM;
        SDL_GetCurrentDisplayMode(0, &DM);
        auto ScreenWidth = DM.w;
        auto ScreenHeight = DM.h;

        Window window("Bumblebee", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 1024, 1024,
                      0);
        Renderer renderer(window, -1, SDL_RENDERER_ACCELERATED);
        renderer.SetDrawBlendMode(SDL_BLENDMODE_BLEND);

        Font font("../DejaVuSans.ttf", 20);

        Texture texture(renderer, "../background.png");

        Player player;

        Map map(texture);
        map.ParseMap("../map.txt");
        map.Advance();

        Game game(renderer, map.mapHeight);

        SDL_Event event;
        bool run = true;
        bool showFPS = true;
        bool finished = false;
        Uint32 loopTicks = 0;
        Uint32 startTicks = SDL_GetTicks();
        Uint32 frames = 0;

        while (run) {
            loopTicks = SDL_GetTicks();

            renderer.Clear();

            while(SDL_PollEvent(&event)) {
                switch (event.type) {
                    case SDL_KEYDOWN:
                        switch (event.key.keysym.sym) {
                            case SDLK_f:
                                showFPS = !showFPS;
                            default:
                                player.HandleInput(event.key.keysym.sym);
                                break;
                        }
                        break;
                    case SDL_QUIT:
                        run = false;
                        break;
                }
            }

            if (frames % 10 == 0) {
                finished = !map.Advance();
            }

            game.RenderMap(map);
            game.RenderPlayer(player);

            if (finished) {
                Texture text(renderer, font.RenderText_Blended("End", {255, 255, 255}));
                renderer.Copy(text, NullOpt, Rect(Point(renderer.GetOutputWidth() / 2, renderer.GetOutputHeight() / 2),
                                                  text.GetSize()));
            }

            if (showFPS) {
                float avgFPS = frames / ((SDL_GetTicks() - startTicks) / 1000.f );
                Texture text(renderer, font.RenderText_Blended(std::to_string(avgFPS), {255, 255, 0}));
                renderer.Copy(text, NullOpt, Rect(Point(10, 10), text.GetSize()));
            }

            renderer.Present();
            frames++;

            const Uint32 frameTicks = SDL_GetTicks() - loopTicks;
            if (frameTicks < TICKS_PER_FRAME) {
                SDL_Delay(TICKS_PER_FRAME - frameTicks);
            }
        }
    } catch (SDL2pp::Exception &e) {
        // Exception stores SDL_GetError() result and name of function which failed
        std::cerr << "Error in: " << e.GetSDLFunction() << std::endl;
        std::cerr << "  Reason: " << e.GetSDLError() << std::endl;
    } catch (std::exception &e) {
        // This also works (e.g. "SDL_Init failed: No available video device")
        std::cerr << e.what() << std::endl;
    }
    return 0;
}
