//
// Created by vojta on 1/12/20.
//

#include "Game.h"

Game::Game(SDL2pp::Renderer &renderer, int mapHeight) {
    this->renderer = &renderer;
    this->blockTexture = new SDL2pp::Texture(renderer, "../block.png");
    this->blockSize = round(renderer.GetOutputHeight() / mapHeight);
    this->renderStartY = ((blockSize * mapHeight) - renderer.GetOutputHeight()) / 2;
}

Game::~Game() {
    delete(blockTexture);
}

void Game::RenderPlayer(const Player& player) {
    renderer->SetDrawColor(player.color);
    renderer->FillRect(SDL2pp::Rect(player.X * blockSize, player.Y * blockSize, blockSize, blockSize));
}

void Game::RenderMap(const Map& map) {
    renderer->Copy(*(map.background), SDL2pp::NullOpt, SDL2pp::Rect(0, 0, renderer->GetOutputWidth(), renderer->GetOutputHeight()));

    for (auto itMap = map.renderMap.begin(); itMap != map.renderMap.end(); ++itMap) {
        int lineIndex = std::distance(map.renderMap.begin(), itMap);

        for (auto itLine = map.renderMap.at(lineIndex).begin(); itLine != map.renderMap.at(lineIndex).end(); ++itLine) {

            int blockIndex = std::distance(map.renderMap.at(lineIndex).begin(), itLine);

            SDL2pp::Rect dest(blockIndex * blockSize, lineIndex * blockSize - renderStartY, blockSize, blockSize);

            switch (map.renderMap.at(lineIndex).at(blockIndex)) {
                case Block::Block:
                    renderer->Copy(*blockTexture, SDL2pp::NullOpt, dest);
                    break;
                case Block::Stop:
                    renderer->SetDrawColor(0, 0, 255);
                    renderer->FillRect(dest);
                    break;
                case Block::Air:
                    break;
            }
        }
    }
}


